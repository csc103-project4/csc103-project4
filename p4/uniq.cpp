#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
	#include <iostream>
#include <string>
using namespace std;

int main() {
  string s;
  string d;
  string f;
  string p="";
  int y=0;
  int count=0;

  string inp;
  getline(cin,inp);


  if (inp=="-c") {

  getline(cin,s);
  for(size_t i=0; i<s.size();i++) {
    if (s[i]==' ' || i==s.size()-1) {
      if (i==s.size()-1) {
        d+=s[i];
        }
      for (size_t k = s.find(d); k!= string::npos; k = s.find(d, k + d.length())) {
        ++count;
      }
      if (y==0) {
        f=d;
        d=" ";
        d+=f;
      }
      if (p.find(d)==string::npos) {
      cout << d << "\t" << count << "\n";}
      count=0;
      p+=d;
      d="";
      y++;


  }
  d+=s[i];

}
}

if (inp=="-d") {
  getline(cin,s);
  for(size_t i=0; i<s.size();i++) {
    if (s[i]==' ' || i==s.size()-1) {
      if (i==s.size()-1) {
        d+=s[i];
        }
      for (size_t k = s.find(d); k!= string::npos; k = s.find(d, k + d.length())) {
        ++count;
      }
      if (y==0) {
        f=d;
        d=" ";
        d+=f;
      }
      if (p.find(d)==string::npos && count>1) {
        for (size_t i=1; i<d.size(); i++){
          if (d[i] == ' ') {
            i++;}
            else {
        cout << d[i];
          printf("%7lu ",count);    } }
        cout<< "\n";

    }
      count=0;
      p+=d;
      d="";
      y++;
    }
    d+=s[i];
  }

}


if (inp=="-u") {
  getline(cin,s);
  for(size_t i=0; i<s.size();i++) {
    if (s[i]==' ' || i==s.size()-1) {
      if (i==s.size()-1) {
        d+=s[i];
        }
      for (size_t k = s.find(d); k!= string::npos; k = s.find(d, k + d.length())) {
        ++count;
      }
      if (y==0) {
        f=d;
        d=" ";
        d+=f;
      }
      if (p.find(d)==string::npos && count==1) {
        for (size_t i=1; i<d.size(); i++){
          if (d[i] == ' ') {
            i++;}
            else {
        cout << d[i];} }
        cout<< "\n";
      }
      count=0;
      p+=d;
      d="";
      y++;
    }

    d+=s[i];
  }

}

    return 0;
}

	return 0;
}
